define(function(require) {
  var breeze = require('breeze');
  var Q = require('Q');
  var DT = breeze.DataType;

  var initialize = function(metadataStore) {
    metadataStore.addEntityType({
      shortName: 'Comment',
      dataProperties: {
        id: {
          dataType: DT.String,
          isPartOfKey: true
        },
        type: {
          dataType: DT.String
        },
        icon: {
          dataType: DT.String
        },
        picture: {
          dataType: DT.String
        },
        link: {
          dataType: DT.String
        },
        name: {
          dataType: DT.String
        },
        message: {
          dataType: DT.String
        },
        created_time: {
          dataType: DT.Date
        }
      }
    });
  };

  var downloadComments = function(manager, objectId) {
    var p = {
      _VOXSUPMETHOD_: 'EDGE'
    };

    q = breeze.EntityQuery.from(`${objectId}/comments`).withParameters(p).toType('Comment');
    return manager.executeQuery(q);
  };

  var downloadComment = function(manager, commentId) {
    var p = {
      _VOXSUPMETHOD_: 'OBJ'
    };

    q = breeze.EntityQuery.from(commentId).withParameters(p).toType('Comment');
    return manager.executeQuery(q);  
  };

  return {
    initialize: initialize,
    downloadComments: downloadComments,
    downloadComment: downloadComment
  };
});
