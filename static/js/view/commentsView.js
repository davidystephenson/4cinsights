define(function(require) {
  var Ember = require('ember');

  var T = Ember.Handlebars.compile('\
    <div class="well"> \
      This section should display a list of recent comments for the active post, and allow the user to \
      delete a comment with the click of a button. \
      <h3>Comments</h3> \
      <table><thead><tr><td>Message</td><td>Delete</td></tr></thead><tbody> \
      {{#each comment in model}} \
        <tr><td>{{comment.message}}</td><td {{action deleteComment comment}}>Kill me</td></tr> \
      {{/each}} \
      </tbody></table> \
    </div> \
  ');

  return Ember.View.extend({
    template: T
  });
});
