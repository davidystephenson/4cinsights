define(function(require) {
  var Ember = require('ember');
  var Q = require('Q');
  var dctx = require('../model/datacontext');

  return Ember.Controller.extend({
    findComments: function(objectId) {
      console.log('findComments objectId test:', objectId)
      return dctx.downloadComments(objectId).then(function (r) {
        return Q.resolve(Ember.getWithDefault(r, 'results', []))
      })
    }
  });
});
